﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Questions
{
    private static readonly Dictionary<FlagAttribute, string> questions;

    static Questions()
    {
        questions = new Dictionary<FlagAttribute, string>();
        questions.Add(FlagAttribute.BlackColor, "Does the flag has BLACK elements?");
        questions.Add(FlagAttribute.BlueColor, "Does the flag has BLUE elements?");
        questions.Add(FlagAttribute.Circle, "Does the flag has CIRCULAR elements?");
        questions.Add(FlagAttribute.Cross, "Does the flag has CROSS-like elements?");
        questions.Add(FlagAttribute.GreenColor, "Does the flag has GREEN elements?");
        questions.Add(FlagAttribute.HorizontalStripes, "Does the flag has HORIZONTAL stripes?");
        questions.Add(FlagAttribute.IrregularShape, "Does the flag has IRREGULAR shapes(or is irregular by itself)?");
        questions.Add(FlagAttribute.Moon, "Does the flag has MOON?");
        questions.Add(FlagAttribute.Star, "Does the flag has STAR(s)? Sun counts as star");
        questions.Add(FlagAttribute.RedColor, "Does the flag has RED elements?");
        questions.Add(FlagAttribute.Triangle, "Does the flag has TRIANGULAR elements?");
        questions.Add(FlagAttribute.MultipleSameColorStripes, "Does the flag has MULTIPLE stripes of the same color?");
        questions.Add(FlagAttribute.VerticalStripes, "Does the flag has VERTICAL stripes?");
        questions.Add(FlagAttribute.WhiteColor, "Does the flag has WHITE elements?");
        questions.Add(FlagAttribute.YellowColor, "Does the flag has YELLOW elements?");
        questions.Add(FlagAttribute.Inscription, "Does the flag has INSCRIPTION ?");
        questions.Add(FlagAttribute.OrangeColor, "Does the flag has ORANGE elements?");
    }

    public static string GetQuestion(FlagAttribute attribute)
    {
        return questions[attribute];
    }
}