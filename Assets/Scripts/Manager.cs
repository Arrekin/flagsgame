﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public GameObject FlagPrefab;
    public GameObject QuestionArea;
    public GameObject ResultArea;

    private List<Register> registers;
    private List<FlagScript> flags;
    private Vector3 spawnPoint;
    private Stage stage;
    private FlagAttribute currentQuestion;

    public void YesButtonClicked()
    {
        if( stage == Stage.Question )
            ReduceFlags(false);
    }

    public void NoButtonClicked()
    {
        if (stage == Stage.Question)
            ReduceFlags(true);
    }

    public void RestartButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

	// Use this for initialization
    private void Start ()
    {
	    registers = new List<Register>();
        flags = new List<FlagScript>();
        spawnPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height * 0.25f))-new Vector3(0, 0, Camera.main.transform.position.z);
        for (var i = 0; i < Enum.GetNames(typeof (FlagAttribute)).Length; ++i)
	    {
	        registers.Add(new Register());
	    }
        GenerateFlags();
        PositionFlags();
    }
	
	// Update is called once per frame
    private void Update ()
    {
        if (stage == Stage.FlagsMove)
        {
            var askQuestion = flags.Aggregate(true, (current, flag) => current & flag.IsMoving);
            if (askQuestion)
            {
                AskQuestion();
            }
        }
	}


    private void GenerateFlags()
    {
        var spriteFlag = Resources.LoadAll<Sprite>("Flags");
        var i = 0;
        CreateFlag("Andorra", spriteFlag[i++], new [] {FlagAttribute.VerticalStripes, FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.IrregularShape,  });
        CreateFlag("United Arab Emirates", spriteFlag[i++], new[] {FlagAttribute.VerticalStripes, FlagAttribute.HorizontalStripes, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.BlackColor, });
        CreateFlag("Afghanistan", spriteFlag[i++], new [] { FlagAttribute.BlackColor, FlagAttribute.RedColor, FlagAttribute.GreenColor,FlagAttribute.WhiteColor, FlagAttribute.IrregularShape, FlagAttribute.Inscription, FlagAttribute.VerticalStripes, });
        CreateFlag("Antigua and Barbuda", spriteFlag[i++], new [] {FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.WhiteColor,FlagAttribute.YellowColor,  FlagAttribute.BlueColor, FlagAttribute.Triangle, FlagAttribute.Star, });
        CreateFlag("Albania", spriteFlag[i++], new [] { FlagAttribute.BlackColor, FlagAttribute.RedColor, FlagAttribute.IrregularShape, });
        CreateFlag("Armenia", spriteFlag[i++], new [] { FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.OrangeColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Angola", spriteFlag[i++], new [] {FlagAttribute.BlackColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.IrregularShape, FlagAttribute.HorizontalStripes, });
        CreateFlag("Argentina", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor,FlagAttribute.YellowColor,  FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Austria", spriteFlag[i++], new[] {FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Australia", spriteFlag[i++], new[] {FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.Star, FlagAttribute.Cross, FlagAttribute.Triangle, });
        CreateFlag("Azerbaijan", spriteFlag[i++], new[] {FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor,  FlagAttribute.HorizontalStripes, FlagAttribute.Moon, FlagAttribute.Star,  });
        CreateFlag("Bosnia and Herzegovina", spriteFlag[i++], new[] {FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.Star, FlagAttribute.Triangle, });
        CreateFlag("Barbados", spriteFlag[i++], new[] {FlagAttribute.BlueColor, FlagAttribute.BlackColor, FlagAttribute.YellowColor, FlagAttribute.IrregularShape, FlagAttribute.VerticalStripes, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Bangladesh", spriteFlag[i++], new[] {FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.Circle, });
        CreateFlag("Belgium", spriteFlag[i++], new[] {FlagAttribute.BlackColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.VerticalStripes, });
        CreateFlag("Burkina Faso", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, });
        CreateFlag("Bulgaria", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Bahrain", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.IrregularShape,  });
        CreateFlag("Burundi", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.Circle, FlagAttribute.Star, });
        CreateFlag("Benin", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.VerticalStripes, FlagAttribute.HorizontalStripes, });
        CreateFlag("Brunei", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.IrregularShape, });
        CreateFlag("Bolivia", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, });
        CreateFlag("Brazil", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.Circle, FlagAttribute.Star, });
        CreateFlag("The Bahamas", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.BlackColor, FlagAttribute.Triangle, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Bhutan", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.OrangeColor,FlagAttribute.WhiteColor,  FlagAttribute.IrregularShape, FlagAttribute.Triangle, });
        CreateFlag("Botswana", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.BlackColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Belarus", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.IrregularShape, FlagAttribute.HorizontalStripes, });
        CreateFlag("Canada", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.IrregularShape, FlagAttribute.VerticalStripes, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Democratic Republic of the Congo", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.Triangle, FlagAttribute.Star, });
        CreateFlag("Central African Republic", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.Star, FlagAttribute.HorizontalStripes, });
        CreateFlag("Republic of the Congo", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.Triangle, });
        CreateFlag("Switzerland", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.Cross, FlagAttribute.IrregularShape, });
        CreateFlag("Ivory Coast", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.OrangeColor, FlagAttribute.VerticalStripes, });
        CreateFlag("Chile", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, });
        CreateFlag("Cameroon", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.Star, FlagAttribute.VerticalStripes, });
        CreateFlag("China", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.Star, });
        CreateFlag("Colombia", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Costa Rica", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Cuba", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Cape Verde", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.Star, FlagAttribute.Circle, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Czech Republic", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, });
        CreateFlag("Germany", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Djibouti", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, FlagAttribute.Star, });
        CreateFlag("Denmark", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.Cross, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Algeria", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.VerticalStripes, FlagAttribute.Star, FlagAttribute.Moon, });
        CreateFlag("Ecuador", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, });
        CreateFlag("Egypt", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape,  });
        CreateFlag("Eritrea", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.Triangle, FlagAttribute.IrregularShape, });
        CreateFlag("Spain", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Ethiopia", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.Star, FlagAttribute.Circle, FlagAttribute.HorizontalStripes, });
        CreateFlag("Finland", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.Cross, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("France", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.VerticalStripes, });
        CreateFlag("United Kingdom", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.Cross, FlagAttribute.Triangle, });
        CreateFlag("Grenada", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.Star, FlagAttribute.Circle, FlagAttribute.IrregularShape, });
        CreateFlag("Ghana", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.BlackColor, FlagAttribute.Star, FlagAttribute.HorizontalStripes, });
        CreateFlag("Guinea", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.VerticalStripes, });
        CreateFlag("Equitorial Guinea", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, FlagAttribute.IrregularShape, });
        CreateFlag("Guatemala", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.VerticalStripes, FlagAttribute.IrregularShape, FlagAttribute.Inscription, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Guyana", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.Triangle, });
        CreateFlag("Honduras", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Croatia", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, });
        CreateFlag("Israel", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.Star, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("India", spriteFlag[i++], new[] { FlagAttribute.OrangeColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.BlueColor, FlagAttribute.Circle, FlagAttribute.HorizontalStripes, });
        CreateFlag("Iraq", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor,  FlagAttribute.BlackColor, FlagAttribute.HorizontalStripes, FlagAttribute.Inscription, FlagAttribute.IrregularShape,  });
        CreateFlag("Iran", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.Inscription, FlagAttribute.IrregularShape, FlagAttribute.HorizontalStripes, });
        CreateFlag("Italy", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.VerticalStripes, });
        CreateFlag("Jamaica", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.Triangle, });
        CreateFlag("Japan", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.Circle, });
        CreateFlag("Kenya", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.BlackColor, FlagAttribute.GreenColor, FlagAttribute.IrregularShape, FlagAttribute.HorizontalStripes, });
        CreateFlag("Kyrgyztan", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.Circle, FlagAttribute.Star, FlagAttribute.IrregularShape, });
        CreateFlag("Kiribati", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.YellowColor, FlagAttribute.IrregularShape, FlagAttribute.Star, });
        CreateFlag("North Korea", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.Circle, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("South Korea", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlackColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.Circle, });
        CreateFlag("Kuwait", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Kazakhstan", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.Star, FlagAttribute.IrregularShape,  FlagAttribute.Circle, });
        CreateFlag("Laos", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.Circle, FlagAttribute.HorizontalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Saint Lucia", spriteFlag[i++], new[] { FlagAttribute.BlueColor,  FlagAttribute.BlackColor, FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.Triangle, });
        CreateFlag("Sri Lanka", spriteFlag[i++], new[] { FlagAttribute.OrangeColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.RedColor, FlagAttribute.IrregularShape, FlagAttribute.VerticalStripes,  });
        CreateFlag("Lithuania", spriteFlag[i++], new[] {FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes,  });
        CreateFlag("Libya", spriteFlag[i++], new[] { FlagAttribute.GreenColor,  });
        CreateFlag("Madagascar", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.VerticalStripes,  });
        CreateFlag("Macau", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.YellowColor, FlagAttribute.IrregularShape, FlagAttribute.Star,  });
        CreateFlag("Mauritania", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.Moon, FlagAttribute.Star, });
        CreateFlag("Maldives", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.Moon, });
        CreateFlag("Malawi", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, });
        CreateFlag("Malaysia", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, FlagAttribute.Moon, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Mozambique", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.BlackColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.Triangle, FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.IrregularShape, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Nairobi", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.BlueColor, FlagAttribute.Triangle, FlagAttribute.Star,  });
        CreateFlag("Nigeria", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.VerticalStripes, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Nepal", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlueColor,FlagAttribute.RedColor,  FlagAttribute.Star, FlagAttribute.Moon, FlagAttribute.IrregularShape, });
        CreateFlag("Oman", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.VerticalStripes, FlagAttribute.IrregularShape, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Papua New Guinea", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.YellowColor, FlagAttribute.BlackColor, FlagAttribute.Star, FlagAttribute.Triangle, FlagAttribute.IrregularShape,  });
        CreateFlag("Philippines", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.YellowColor,  FlagAttribute.Triangle, FlagAttribute.Star, FlagAttribute.HorizontalStripes, });
        CreateFlag("Poland", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes,  });
        CreateFlag("Palau", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.Circle,  });
        CreateFlag("Paraguay", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.Circle, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, });
        CreateFlag("Romania", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.VerticalStripes, });
        CreateFlag("Russia", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Rwanda", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.GreenColor, FlagAttribute.Star, FlagAttribute.Circle, FlagAttribute.HorizontalStripes,  });
        CreateFlag("Saudi Arabia", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.Inscription, });
        CreateFlag("Sweden", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, FlagAttribute.Cross, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Singapore", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Moon, FlagAttribute.Star, });
        CreateFlag("Slovakia", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.RedColor, FlagAttribute.IrregularShape, FlagAttribute.HorizontalStripes, FlagAttribute.Cross, });
        CreateFlag("Sierra Leone", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Somalia", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.Star, });
        CreateFlag("Suriname", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("South Sudan", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.YellowColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.Triangle, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Syria", spriteFlag[i++], new[] { FlagAttribute.BlackColor, FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.GreenColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, });
        CreateFlag("Tajikistan", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, FlagAttribute.Star,  });
        CreateFlag("East Timor", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.YellowColor, FlagAttribute.WhiteColor, FlagAttribute.Star, FlagAttribute.Triangle, });
        CreateFlag("Tunisia", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.Circle, FlagAttribute.Moon, FlagAttribute.Star, });
        CreateFlag("Turkey", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.WhiteColor, FlagAttribute.Moon, FlagAttribute.Star, });
        CreateFlag("Trinidad and Tobago", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.WhiteColor, FlagAttribute.Triangle, });
        CreateFlag("Tanzania", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.YellowColor, FlagAttribute.BlackColor, FlagAttribute.BlueColor, FlagAttribute.Triangle, });
        CreateFlag("Ukraine", spriteFlag[i++], new[] { FlagAttribute.BlueColor, FlagAttribute.YellowColor, FlagAttribute.HorizontalStripes, });
        CreateFlag("Uganda", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.RedColor, FlagAttribute.BlackColor, FlagAttribute.WhiteColor, FlagAttribute.HorizontalStripes, FlagAttribute.IrregularShape, FlagAttribute.Circle, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("United States of America", spriteFlag[i++], new[] { FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes, });
        CreateFlag("Uzbekistan", spriteFlag[i++], new[] { FlagAttribute.GreenColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.BlueColor, FlagAttribute.HorizontalStripes, FlagAttribute.Moon, FlagAttribute.Star, FlagAttribute.MultipleSameColorStripes,  });
        CreateFlag("Venezuela", spriteFlag[i++], new[] { FlagAttribute.YellowColor, FlagAttribute.BlueColor, FlagAttribute.WhiteColor, FlagAttribute.RedColor, FlagAttribute.HorizontalStripes, FlagAttribute.Star, });
        CreateFlag("Zambia", spriteFlag[i++], new[] { FlagAttribute.RedColor, FlagAttribute.GreenColor, FlagAttribute.BlackColor, FlagAttribute.OrangeColor, FlagAttribute.VerticalStripes, FlagAttribute.IrregularShape, });
    }

    private void CreateFlag(string name, Sprite sprite, FlagAttribute[] attributes)
    {
        var flag = ((GameObject) Instantiate(FlagPrefab,spawnPoint,Quaternion.identity)).GetComponent<FlagScript>();
        flag.Initialize(name, sprite, attributes);
        RegisterFlag(flag, attributes);
        flags.Add(flag);
    }

    private void PositionFlags()
    {
        stage = Stage.FlagsMove;
        var flagsPerLine = (int) Mathf.Max(1,Mathf.Min(flags.Count/2F, 15));
        var margin = (int) (Screen.width/2f - 80*(flagsPerLine - 1)/2f);
        var baseHeight = Mathf.Min(Screen.height-40, Screen.height/2f + 60f*(flags.Count/flagsPerLine - 1)); 
        for (var i = 0; i < flags.Count; ++i)
        {
            flags[i].SetDestinationPoint(Camera.main.ScreenToWorldPoint(new Vector3(margin+(i%flagsPerLine)*80,baseHeight-70*(i/flagsPerLine),0))-new Vector3(0,0,Camera.main.transform.position.z));
        }
    }

    private void RegisterFlag(FlagScript flag, FlagAttribute[] attributes)
    {
        foreach (var attribute in attributes)
        {
            registers[(int) attribute].RegisterFlag(flag);
        }
    }

    private void AskQuestion()
    {
        stage = Stage.Question;
        if (flags.Count == 1)
        {
            ResultArea.GetComponentInChildren<Text>().text = flags[0].name;
            ResultArea.SetActive(true);
            return;
        }
        var half = flags.Count/2f;
        var bestQuestion = 0;
        for (var i = 1; i < registers.Count; ++i)
        {
            if (Math.Abs(registers[i].Size - half) < Math.Abs(registers[bestQuestion].Size - half))
                bestQuestion = i;
        }
        currentQuestion = (FlagAttribute)bestQuestion;
        QuestionArea.GetComponentInChildren<Text>().text = Questions.GetQuestion(currentQuestion);
        QuestionArea.SetActive(true);
    }

    private void ReduceFlags(bool withAttribute)
    {
        for (var i = 0; i < flags.Count; ++i)
        {
            var hasAttribute = registers[(int) currentQuestion].ContainsFlag(flags[i]);
            if (!(withAttribute ^ hasAttribute))
            {
                foreach (var register in registers)
                {
                    register.DeregisterFlag(flags[i]);
                }
                flags[i].Kill();
                flags.RemoveAt(i);
                --i;
            }
        }
        PositionFlags();
    }
    private enum Stage
    {
        FlagsMove,
        Question
    }
}
