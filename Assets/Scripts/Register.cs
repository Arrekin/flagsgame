﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Register
{
    public int Size { get { return flags.Count; } }

    private readonly List<FlagScript> flags;

    public Register()
    {
        flags = new List<FlagScript>();
    }

    public void RegisterFlag(FlagScript flag)
    {
        flags.Add(flag);
    }

    public void DeregisterFlag(FlagScript flag)
    {
        if( flags.Contains(flag))
            flags.Remove(flag);
    }

    public bool ContainsFlag(FlagScript flag)
    {
        return flags.Contains(flag);
    }
}