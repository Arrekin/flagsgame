﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlagScript : MonoBehaviour
{
    public float Speed = 5;
    public Text CountryName;

    public FlagAttribute[] Attributes { get; private set; }

    public bool IsMoving
    {
        get { return transform.position == target; }
    }

    private Vector3 target;
    private bool dyingMode;

    private const float Epsilon = 0.01f;
    private static readonly Vector3 DyingSpeed = new Vector3(1f,1f,0);

    public void Initialize(string name, Sprite sprite, FlagAttribute[] attributes)
    {
        this.name = name;
        GetComponent<SpriteRenderer>().sprite = sprite;
        Attributes = attributes;
        target = transform.position;
        dyingMode = false;
        CountryName.text = "";
    }

    public void SetDestinationPoint(Vector3 target)
    {
        this.target = target;
    }

    public void Kill()
    {
        dyingMode = true;
    }
	
	// Update is called once per frame
	private void Update ()
    {
	    if (Mathf.Abs(target.x - transform.position.x) > Epsilon || Mathf.Abs(target.y - transform.position.y) > Epsilon)
	    {
	        transform.position = Vector3.MoveTowards(transform.position, target, Speed*Time.deltaTime*2);
	    }
	    else
	    {
	        transform.position = target;
	    }

	    if (dyingMode)
	    {
	        transform.localScale -= Time.deltaTime * DyingSpeed;
	        transform.position += Vector3.up*Time.deltaTime*Speed*2.5f;
	        if (transform.localScale.x <= 0  )
	        {
	            Destroy(gameObject);
	        }
	    }
	}

    private void OnMouseEnter()
    {
        CountryName.text = name;
    }

    private void OnMouseExit()
    {
        CountryName.text = "";
    }
}
